/// <reference types="Cypress" />

class UI {

      email;
      password;

     constructor( email, password) {
        this.email = email;
        this.password = password;
    }
}

let  badLogin  = new UI('recruitment_test@interactio.io', 'tsst1')
let goodLogin = new UI('recruitment_test@interactio.io', 'WelcomeOnBoard!')

describe('test if login will be denied with incorrect credentials', () => {
    
    it('loads url',  () => {
        cy.visit('https://dev-panel.interactio.io/login');
    })

    it('login failled', () => {
        cy.get('input[type="email"]').clear().type(badLogin.email);
        cy.get('input[type="password"]').clear().type(badLogin.password);
        // right click into double click
        cy.get(':nth-child(4) > .alignRight > .button').rightclick({force: true});
        cy.get(':nth-child(4) > .alignRight > .button').dblclick({force: true});
        
        // checking if invalid email message will pop up, jquery way
        cy.get('.loginView > :nth-child(4) > :nth-child(1)').then($element => {
            if ($element.find('.colored')) {
                console.log(">>> .color element showed up as expected");
            }
        })
        
        // ASSERT message
        cy.get('.colored').should('be.visible');
    })    
})

describe('successfully logs in and does a few navigation laps inside dashboard', () => {

    it('login success', () => {
        cy.get(':nth-child(2) > .ng-valid').clear().type(goodLogin.email);
        cy.get('.loginView > :nth-child(3) > .ng-valid').clear().type(goodLogin.password);
        cy.get(':nth-child(4) > .alignRight > .button').click('bottomRight');

        // checking to see if login was  successfull
        cy.url().should('include', '/dashboard');
    })

    it('user managment', () => {
        // entering user management
        cy.get('.menu').click().get('ul > li:nth-child(4)').click();
        //clicking on eddit button
        cy.get(':nth-child(2) > .w10 > .rightSpace').click();
        //double click producess two pop ups
        cy.get('.visible > .dialog > .alignRight > .button').dblclick();
                          
    })

    it('logout', () => {
        cy.get('.menu').click().get('ul > li:nth-child(6)').click();
    })
    
})





